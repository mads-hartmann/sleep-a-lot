const express = require('express');
const marked = require('marked');
const fs = require('fs');

const app = express();

marked.setOptions({
  sanitize: true,
});

app.get('/', function(request, response) {
  const path = `${__dirname}/README.md`;
  fs.readFile(path, 'utf8', function(err, contents) {
    response.send(marked(contents));  
  });
});

app.get('/sleep', async (request, response) => {
  const ms = parseInt(request.query.ms, 10) || 70000
  request.setTimeout(ms + 1000)
  await new Promise((resolve, reject) => setTimeout(() => resolve(), ms));
  response.send(`slept ${ms}ms`);
});

app.get('/timeout', async (request, response) => {
  const ms = parseInt(request.query.ms, 10) || 1000
  request.setTimeout(ms)
  await new Promise((resolve, reject) => setTimeout(() => resolve(), ms * 2));
  response.send(`What? Should have timed out after ${ms}ms`);
});

const listener = app.listen(process.env.PORT, function() {
  console.log('Your app is listening on port ' + listener.address().port);
});
