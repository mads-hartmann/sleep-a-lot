# Sleep a lot

A simple node server that provides endpoints for testing various time related edge-cases.

| url | description | example |
| - | - | - |
| `/sleep?ms=<NUMBER>`| Sleep for `ms` milliseconds and then return a response. Useful for testing the various timeouts that we have when routing a request all the way from the client to the Glitch project. | https://sleep-a-lot.glitch.me/sleep?ms=1000 |
| `/timeout?ms=<NUMBER>` | The application closes the connection after `ms` milliseconds. Useful for testing how we handle projects that close their connection before our timeout is triggered. | https://sleep-a-lot.glitch.me/timeout?ms=1000 |

*This line was added in Gitlab to try their push mirroring*